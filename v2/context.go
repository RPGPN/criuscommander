package criuscommander

import (
	"context"
	"github.com/bwmarrin/discordgo"
)

type MessageContext struct {
	PlatformData interface{}
	Context      context.Context
	Message
}

func (m *MessageContext) Send(message interface{}) (interface{}, error) {
	platform := m.Context.Value("platform").(PlatformType)

	var sendTo interface{}

	switch platform {
	case PlatformDiscord:
		sendTo = m.PlatformData.(*discordgo.MessageCreate).ChannelID
	case PlatformGlimesh, PlatformTwitch:
		sendTo = m.RealmID
	}

	return m.SendTo(sendTo, message)
}

func (m *MessageContext) SendTo(channelID interface{}, message interface{}) (interface{}, error) {
	commander := m.Context.Value("commander").(*Commander)

	platform := m.Context.Value("platform").(PlatformType)

	return commander.SendMessage(platform, channelID, message)
}
