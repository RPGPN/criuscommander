package criuscommander

type MessageUser struct {
	ID       string
	Username string

	IsMod      func() bool
	IsStreamer func() bool
}

type Message struct {
	RealmID string
	Author  MessageUser
}
