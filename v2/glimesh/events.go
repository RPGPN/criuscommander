package glimesh

import (
	"context"
	"github.com/asaskevich/EventBus"
	"gitlab.com/crius-bots/criuscommander/v2"
	"gitlab.com/ponkey364/golesh-chat"
)

const (
	EventMessageCreate = "Commander.Glimesh.Message:Create"
)

func (g *Glimesh) RegisterEvents(publisher EventBus.BusPublisher) error {
	// we store the publisher and trigger it below in the message handler in case stuff needs non-command stuff
	// (for link permissions or whatever)
	g.bus = publisher
	return nil
}

func (g *Glimesh) RegisterMessageHandler(f func(string, *criuscommander.MessageContext)) error {
	g.session.SetOnMessageHandler(func(s *goleshchat.Session, m *goleshchat.ChatMessage) {
		ctx := context.WithValue(g.ctx, "platform", g.GetPlatformType())
		mc := &criuscommander.MessageContext{
			PlatformData: m,
			Context:      ctx,
			Message:      *makeMessage(m),
		}

		g.bus.Publish(EventMessageCreate, mc)

		f(m.Message, mc)
	})

	return nil
}

func makeMessage(m *goleshchat.ChatMessage) *criuscommander.Message {
	return &criuscommander.Message{
		RealmID: m.Channel.ID,
		Author: criuscommander.MessageUser{
			ID:         m.User.ID,
			Username:   m.User.Username,
			IsMod:      func() bool { return false }, // we cannot find this on Glimesh yet. (easily)
			IsStreamer: func() bool { return m.Channel.Streamer.ID == m.User.ID },
		},
	}
}
