package glimesh

import (
	"gitlab.com/crius-bots/criuscommander/v2"
)

func (g *Glimesh) GenHelp(_ interface{}, m *criuscommander.MessageContext) error {
	m.Send("Crius help is available at https://crius.rpgpn.co.uk/docs/commands/")
	return nil
}
