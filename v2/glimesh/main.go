package glimesh

import (
	"context"
	"github.com/asaskevich/EventBus"
	"gitlab.com/crius-bots/criuscommander/v2"
	"gitlab.com/ponkey364/golesh-chat"
)

func New() criuscommander.Platform {
	return &Glimesh{}
}

type Glimesh struct {
	session   *goleshchat.Session
	ctx       context.Context
	onMessage func(*goleshchat.Session, *goleshchat.ChatMessage)
	bus       EventBus.BusPublisher
}

func (g *Glimesh) Setup(token string) error {
	s := goleshchat.New(token, nil)
	g.session = s

	return nil
}

func (g *Glimesh) GetPlatformType() criuscommander.PlatformType {
	return criuscommander.PlatformGlimesh
}

func (g *Glimesh) GetClient() interface{} {
	return g.session
}

func (g *Glimesh) Open(ctx context.Context) error {
	g.ctx = ctx
	err := g.session.Open()
	if err != nil {
		return err
	}

	return nil
}

func (g *Glimesh) Close() error {
	g.session.Close()
	return nil
}

func (g *Glimesh) JoinChannel(channel interface{}) error {
	g.session.ConnectToChannel(channel.(int))
	return nil
}

// isn't implemented in Glimesh's API currently
func (g *Glimesh) LeaveChannel(_ interface{}) error {
	return nil
}
