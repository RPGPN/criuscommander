package criuscommander

func (c *Commander) Help(m *MessageContext, args []string) error {
	var data interface{}
	platform := m.Context.Value("platform").(PlatformType)

	if len(args) == 0 {
		m := map[string]*CommandGroupContainer{}
		for k, v := range c.commandGroups {
			if v.Info.HideInHelp {
				continue
			}
			if v.Info.SupportedPlatforms&platform == platform {
				m[k] = v
			}
		}

		data = m
	}
	if len(args) == 1 {
		if group, ok := c.commandGroups[args[0]]; ok {
			// we've got a command group
			// should we show it?
			if group.Info.HideInHelp {
				// no.
				return nil
			}

			// yes, but does it support this platform?
			if group.Info.SupportedPlatforms&platform == platform {
				data = group
			}
		}
		// okay, maybe this is a command?
		if cmd, ok := c.commands[args[0]]; ok {
			// it is, noice
			if cmd.Info.SupportedPlatforms&platform == platform {
				data = cmd
			}
		}
	}

	if len(args) > 1 {
		data = findSubcommandHelp(c.commands[args[0]], args[1:])
	}

	err := c.registeredPlatforms[platform].GenHelp(data, m)
	return err
}

func findSubcommandHelp(upper *Command, args []string) *Command {
	if cmd, ok := upper.Subcommands[args[0]]; ok {
		// is there any more args?
		if len(args) > 1 {
			modCmd := *findSubcommandHelp(cmd, args[1:])

			modCmd.Info.Activator = upper.Info.Activator + " " + modCmd.Info.Activator
			return &modCmd
		}

		modCmd := *cmd
		modCmd.Info.Activator = upper.Info.Activator + " " + modCmd.Info.Activator

		return &modCmd
	}

	return upper
}
