package criuscommander

import (
	"errors"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	EventCommanderClose = "commander:close"
	MaxDepth            = 8
)

func (c *Commander) onMessage(message string, m *MessageContext) {
	defer func() {
		if r := recover(); r != nil {
			logrus.Errorf("Recovered from %s", r)
		}
	}()

	// this should be a command
	if strings.HasPrefix(message, c.ccConfig.Prefix) {
		args := ArgsSplitter(message[1:])

		if len(args) == 0 {
			// the message is probably just *the prefix* and no other text
			return
		}

		// call the BeforeCommandCheck guard
		if c.GuardBeforeCommandCheck != nil {
			if !c.GuardBeforeCommandCheck(m, args[0], args[1:]) {
				return
			}
		}

		if command, ok := c.commands[args[0]]; ok {
			didExec, err := checkCommand(c, command, args, m, 0)
			if err != nil {
				c.OnError(err)
			}
			// we should call the PostCommandCheck below if we didn't exec a cmd (or get false from BeforeCommandCheck)
			if didExec {
				return
			}
		}

		// call the PostCommandCheck guard (no command has been found)
		if c.GuardPostCommandCheck != nil {
			c.GuardPostCommandCheck(m, args[0], args[1:])
		}
	}
}

func checkCommand(c *Commander, command *Command, args []string, m *MessageContext, depth uint) (bool, error) {
	if depth >= MaxDepth {
		return false, errors.New("max subcommand depth reached")
	}

	// check if the platform is correct
	platform := m.Context.Value("platform").(PlatformType)
	if command.Info.SupportedPlatforms&platform != platform {
		// this platform isn't supported
		return false, nil
	}

	// are we trying to access a subcommand?
	if len(args) > 1 && (command.Subcommands != nil && len(command.Subcommands) >= 1) {
		// we could be?
		if sub, ok := command.Subcommands[args[1]]; ok {
			didExec, err := checkCommand(c, sub, args[1:], m, depth+1)
			return didExec, err
		}
	}

	// call the BeforeCommandExec guard
	if c.GuardBeforeCommandExec != nil {
		if !c.GuardBeforeCommandExec(m, args[0], args[1:]) {
			return true, nil
		}
	}

	if command.Handler == nil {
		return false, nil
	}

	err := command.Handler(m, args[1:])
	return true, err
}
