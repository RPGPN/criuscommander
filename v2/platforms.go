package criuscommander

import (
	"context"
	"github.com/asaskevich/EventBus"
)

type PlatformType uint

func (pt PlatformType) ToString() string {
	switch pt {
	case PlatformDiscord:
		return "DISCORD"
	case PlatformGlimesh:
		return "GLIMESH"
	case PlatformTwitch:
		return "TWITCH"
	default:
		return ""
	}
}

const (
	PlatformDiscord PlatformType = 1 << iota
	PlatformGlimesh
	PlatformTwitch
)

type Platform interface {
	// The string here will be a token (since that's what most platforms use)
	Setup(string) error

	GetPlatformType() PlatformType

	// sometimes we have a need to access the underlying funcs for a platform (i.e. DiscordGo's Session.State)
	GetClient() interface{}

	// Register events with your library like OnServerJoin or OnMemberJoin or the like.
	// The events from your library should be passed through EventBus using BusPublisher.Publish
	RegisterEvents(EventBus.BusPublisher) error
	RegisterMessageHandler(func(string, *MessageContext)) error

	// Begin the connection.
	// Context is passed so that you can store it and pass it down to events
	Open(context.Context) error

	// Close the connection.
	// Fire off *your* close event, but not a EventCommanderClose.
	Close() error

	// Allow each platform to generate its own help messages (i.e. discord can use embeds, twitch can send a link &c.)
	// The interface will be a map[string]*Command or a map[string]*CommandGroupContainer depending on the passed param
	GenHelp(interface{}, *MessageContext) error

	// Join another channel by the given name/id/whatever.
	JoinChannel(interface{}) error

	// Leave a channel with given name/id
	LeaveChannel(interface{}) error
}
