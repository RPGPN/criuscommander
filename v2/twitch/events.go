package twitch

import (
	"context"
	"github.com/asaskevich/EventBus"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/crius-bots/criuscommander/v2"
)

const (
	EventTwitchConnected = "Commander.Twitch:Connect"
	EventTwitchNotice    = "Commander.Twitch:Notice"
	EventChatClear       = "Commander.Twitch.Chat:Clear"
	EventMessageClear    = "Commander.Twitch.Message:Clear"
	EventPrivateMessage  = "Commander.Twitch.Message.Private:Create"
	EventWhisperMessage  = "Commander.Twitch.Message.Whisper:Create"
	EventRoomState       = "Commander.Twitch.Room.State"
	EventUserJoin        = "Commander.Twitch.User:Join"
	EventUserPart        = "Commander.Twitch.User:Part"
	EventUserNotice      = "Commander.Twitch.User.Notice"
	EventUserState       = "Commander.Twitch.User.State"
	EventGlobalUserState = "Commander.Twitch.User.Global.State"
)

func (t *Twitch) RegisterEvents(publisher EventBus.BusPublisher) error {
	t.bus = publisher

	t.session.OnConnect(func() {
		publisher.Publish(EventTwitchConnected)
	})
	t.session.OnNoticeMessage(func(m twitch.NoticeMessage) {
		publisher.Publish(EventTwitchNotice, &m)
	})
	t.session.OnClearChatMessage(func(m twitch.ClearChatMessage) {
		publisher.Publish(EventChatClear, &m)
	})
	t.session.OnClearMessage(func(m twitch.ClearMessage) {
		publisher.Publish(EventMessageClear, &m)
	})
	t.session.OnWhisperMessage(func(m twitch.WhisperMessage) {
		publisher.Publish(EventWhisperMessage, &m)
	})
	t.session.OnRoomStateMessage(func(m twitch.RoomStateMessage) {
		publisher.Publish(EventRoomState, &m)
	})
	t.session.OnUserJoinMessage(func(m twitch.UserJoinMessage) {
		publisher.Publish(EventUserJoin, &m)
	})
	t.session.OnUserPartMessage(func(m twitch.UserPartMessage) {
		publisher.Publish(EventUserPart, &m)
	})
	t.session.OnUserNoticeMessage(func(m twitch.UserNoticeMessage) {
		publisher.Publish(EventUserNotice, &m)
	})
	t.session.OnUserStateMessage(func(m twitch.UserStateMessage) {
		publisher.Publish(EventUserState, &m)
	})
	t.session.OnGlobalUserStateMessage(func(m twitch.GlobalUserStateMessage) {
		publisher.Publish(EventGlobalUserState, &m)
	})

	return nil
}

func (t *Twitch) RegisterMessageHandler(f func(string, *criuscommander.MessageContext)) error {
	t.session.OnPrivateMessage(func(message twitch.PrivateMessage) {
		ctx := context.WithValue(t.ctx, "platform", t.GetPlatformType())
		mc := &criuscommander.MessageContext{
			PlatformData: &message,
			Context:      ctx,
			Message:      *makeMessage(&message),
		}

		t.bus.Publish(EventPrivateMessage, mc)

		f(message.Message, mc)
	})

	return nil
}

func makeMessage(m *twitch.PrivateMessage) *criuscommander.Message {
	return &criuscommander.Message{
		RealmID: m.Channel,
		Author: criuscommander.MessageUser{
			ID:         m.User.ID,
			Username:   m.User.Name,
			IsMod:      func() bool { _, ok := m.User.Badges["moderator"]; return ok },
			IsStreamer: func() bool { _, ok := m.User.Badges["broadcaster"]; return ok },
		},
	}
}
