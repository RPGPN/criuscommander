package twitch

import (
	"context"
	"github.com/asaskevich/EventBus"
	"github.com/gempir/go-twitch-irc/v2"
	"gitlab.com/crius-bots/criuscommander/v2"
	"strings"
)

func New() criuscommander.Platform {
	return &Twitch{}
}

type Twitch struct {
	session *twitch.Client
	ctx     context.Context
	bus     EventBus.BusPublisher
}

func (t *Twitch) Setup(token string) error {
	// the username and token should be smashed together like username@oauth:token
	// it's not pretty but I don't have a way of getting other data in atm.
	splitToken := strings.Split(token, "@")

	t.session = twitch.NewClient(splitToken[0], splitToken[1])

	return nil
}

func (t *Twitch) GetPlatformType() criuscommander.PlatformType {
	return criuscommander.PlatformTwitch
}

func (t *Twitch) GetClient() interface{} {
	return t.session
}

func (t *Twitch) Open(ctx context.Context) error {
	t.ctx = ctx
	go t.session.Connect()

	return nil
}

func (t *Twitch) Close() error {
	return t.session.Disconnect()
}

func (t *Twitch) JoinChannel(i interface{}) error {
	t.session.Join(i.(string))

	return nil
}

func (t *Twitch) LeaveChannel(i interface{}) error {
	t.session.Depart(i.(string))

	return nil
}
