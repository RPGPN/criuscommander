package criuscommander

import (
	"strings"
	"unicode/utf8"
)

func ArgsSplitter(in string) []string {
	parts := []string{}

	isInQuote := false
	isEscaping := false
	shouldSkip := false

	start := 0

	for i, c := range in {
		if shouldSkip {
			shouldSkip = false
			continue
		}
		if c == ' ' && !isInQuote {
			parts = append(parts, in[start:i])
			start = i + 1
			continue
		}
		if c == '"' && !isEscaping {
			if isInQuote {
				isInQuote = false
				parts = append(parts, strings.TrimSpace(in[start+1:i]))
				start = i + 2
				shouldSkip = true
				continue
			} else {
				start = i
				isInQuote = true
				continue
			}
		}
		if c == '\\' && !isEscaping {
			isEscaping = true
			continue
		}

		runeLen := utf8.RuneLen(c)

		if i == len(in)-runeLen {
			parts = append(parts, in[start:])
			continue
		}

		isEscaping = false
	}

	return parts
}

func makeSubcommands(subcommands []*PJCommand, getHandler GetCommandHandler, upperName string) (map[string]*Command, error) {
	cmdMap := map[string]*Command{}

	for _, subcommand := range subcommands {
		accessor := upperName + "/" + subcommand.HandlerName

		handler, err := getHandler(accessor)
		if err != nil {
			return nil, err
		}

		// does this subcommand have subcommands?
		var deepCommands map[string]*Command
		if subcommand.Subcommands != nil && len(subcommand.Subcommands) > 0 {
			deepCommands, err = makeSubcommands(subcommand.Subcommands, getHandler, accessor)
			if err != nil {
				return nil, err
			}
		}

		cmdMap[subcommand.Activator] = &Command{
			Handler:     handler,
			Info:        subcommand,
			Subcommands: deepCommands,
		}
	}

	return cmdMap, nil
}
