package criuscommander

import (
	"context"
	"errors"
	"fmt"
	"path"
	"plugin"
)

const NativePluginLoader PluginType = "uk.co.rpgpn.crius.commander.loader:native"

type NewNativeLoader struct{}

func (n *NewNativeLoader) GetTypeName() PluginType {
	return NativePluginLoader
}

func (n *NewNativeLoader) New(ctx context.Context) Loader {
	fmt.Println("The NativeLoader is deprecated")
	return &NativeLoader{Context: ctx}
}

type NativeLoader struct {
	Context  context.Context
	commands map[string]CommandHandler
}

func (n *NativeLoader) LoadPlugin(name string) error {
	p := path.Join("plugins", name, name+".so")

	pl, err := plugin.Open(p)
	if err != nil {
		return err
	}

	setup, err := pl.Lookup("Setup")
	if err != nil {
		return err
	}

	commands, err := setup.(func(context.Context) (map[string]CommandHandler, error))(n.Context)
	if err != nil {
		return err
	}

	n.commands = commands

	return nil
}

func (n *NativeLoader) GetCommandHandler(name string) (CommandHandler, error) {
	if h, ok := n.commands[name]; ok {
		return h, nil
	}
	return nil, errors.New(fmt.Sprintf("no command %s found", name))
}
