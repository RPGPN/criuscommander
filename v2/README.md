# Crius Commander v2

## Usage
1. Install with `go get gitlab.com/crius-bots/criuscommander/v2`
2. Make a `map[criuscommander.PlatformType]string` to hold your access tokens
    - This should look like below
         ```go
        tokens := map[criuscommander.PlatformType]string{
          criuscommander.PlatformDiscord: os.Getenv("discord_token")
        }
        ```
3. Init the bot with `criuscommander.NewCommander`, passing in a `*criuscommander.CCConfig`
    - Unless you're using a custom loader (see [loaders](#loaders)), pass `criuscommander.DefaultLoaderChain`
      (or nothing at all if you're not using the loader system)
    - Pass any [platforms](#platform-support) you want to use into Platforms, i.e. 
      `Platforms: []criuscommander.Platform{discord.New()}`
    - If you have plugins you want to load automatically (recommended) then set `AutoloadPlugins` to `true`
         - note that this will not do anything unless you pass loaders in as above.
    - Optionally pass a context as the second parameter. This is useful if you have a struct you want to share like a
      database or permissions checking functions
4. Call `.Open()` when you're ready to connect
   - note that Open does not block.
5. Call `.Close()` when you're finished.

## Writing Plugins
First, make a folder for your plugin, in there, create a file called `plugin.json`.

If your editor supports it, I would recommend setting the file's JSON schema to this repo's `schema.json`

Fill the data in for the plugin. You may have to provide a plugin type, which Commander uses to pick which loader to
use.[^1] Refer to the documentation for the loader for this value. You will also have to supply the supported platforms.
If you are only supporting one platform, just use its number. If you are supporting many, add all the numbers up and
Commander will make sure only the correct platforms call your plugin functions.

The values for each platform are provided below and in `platforms.go`

You will also have to make an entry in plugin.json for every command.

- Name and Help are self-explanatory
- Supported_platforms is the same as above. Useful if you have one command in a plugin that only supports one of the
plugin's platforms
- Activator is what users will use after the prefix to call your command. i.e. Help's activator is "help"
- handler_name is used to map commands in this file to compiled functions.

Once you've written your plugin, put both the code for the loader and the plugin.json in a folder
called `plugins/[pluginname]`.

The code that you provide will depend on the loader (and even if you're using one or not). If you are using a loader,
refer to its documentation for what to ship.

If you're *manually* loading plugins (see below), use a function which takes a `context.Context` and returns a
`map[string]criuscommander.CommandHandler` and an error (unless you're doing something spectacular you can hardcode
this to `nil`). This map should have the handler_name as the key and then the function you wish to call as the value.

[^1]: You don't have to fill this field if you're loading plugins through `SetupPluginFromConfig`, which is the
recommended way of doing so currently.

### Using Plugins
If you're using a loader, all you have to do is pass it into `Loaders` in config and set `AutoloadPlugins` to `true`.

If you're manually loading, call `(*Commander).SetupPluginFromConfig`, passing in a function (see Writing Plugins) and
a config


### The Native Loader
**Note that the native loader uses Go plugins which do not support Windows yet**  
**I would strongly advise against using this, as Go is _incredibly_ picky about plugins and versioning**, plugin authors
are advised to just publish their source and consumers are advised to import

To compile your plugin for the native loader, first ensure the package at the top of every file for the plugin is main.
This is a restriction of Go's Plugin code.

You will need a `Setup` function which takes a `context.Context` and returns a `map[string]criuscommander.CommandHandler`
and an error (unless you're doing something spectacular you can hardcode this to `nil`).
This map should have the handler_name as the key and then the function you wish to call as the value.

The functions you make should have the signature `FunctionName (*criuscommander.MessageContext, []string) error` where
the second parameter is the args, split per `utils.ArgsSplitter`

When you've written your functions, you need to build as a go plugin, which can be done by using `go build -buildmode=plugin`.

### Context
The context you pass into NewCommander is augmented with every loaded platform's client (by their `Platform.ToString()`)
(this means that you can get direct access to DiscordGo to do reactions, for example.), a reference to the event bus
(see below), and the OtherConfig map you passed into (under name `config`)

### Events
This is useful if you need to subscribe to a certain library's events, i.e. DiscordGo's OnReactionAdd.

You can do this by getting `bus` from context and casting it to `EventBus.BusSubscriber` and then calling `.Subscribe()`
with a string (which will be provided by the platform), and a function

## Platform Support

- [1] Discord
- [2] Glimesh
- [4] Twitch

Other platforms will come in the future (including Guilded as soon as their bot API launches.)

Youtube will not be supported until they add an actually usable chat API (ie. websockets, because I'm not polling.)

## Loaders
Loaders are a way of implementing different ways of setting up a plugin, or to even embed a scripting language for use
in commands.  
Included is a *deprecated* loader that uses Go's Plugin system.

Due to restrictions with Go's plugin system, Windows is not supported when using the native loader.

To implement your own loader, see `loader.go` which contains the interface.
