package criuscommander

import "context"

var DefaultLoaderChain = []LoaderFactory{}

type GetCommandHandler func(string) (CommandHandler, error)

type LoaderFactory interface {
	GetTypeName() PluginType

	// We pass context in here just in case you need to build some stuff up (the native loader does)
	New(context.Context) Loader
}

type Loader interface {
	LoadPlugin(string) error
	GetCommandHandler(string) (CommandHandler, error)
}
