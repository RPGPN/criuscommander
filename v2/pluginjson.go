package criuscommander

import (
	"encoding/json"
	"io/ioutil"
	"path"
)

type PluginType string

type PJCommand struct {
	Name               string
	Activator          string
	Help               string
	HandlerName        string       `json:"handler_name"`
	SupportedPlatforms PlatformType `json:"supported_platforms"`
	Subcommands        []*PJCommand
}

type PluginJSON struct {
	Name               string
	Creator            string
	License            string
	URL                string `json:"url"`
	Description        string
	PluginType         PluginType   `json:"plugin_type"`
	SupportedPlatforms PlatformType `json:"supported_platforms"`
	Commands           []*PJCommand
	HideInHelp         bool `json:"hide_in_help"`
}

func getPluginJSON(pluginName string) (*PluginJSON, error) {
	pjPath := path.Join("plugins", pluginName, "plugin.json")

	contents, err := ioutil.ReadFile(pjPath)
	if err != nil {
		return nil, err
	}

	data := &PluginJSON{}

	err = json.Unmarshal(contents, data)
	if err != nil {
		return nil, err
	}

	return data, nil
}
