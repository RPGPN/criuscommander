module gitlab.com/crius-bots/criuscommander/v2

go 1.16

require (
	github.com/asaskevich/EventBus v0.0.0-20200907212545-49d423059eef
	github.com/bwmarrin/discordgo v0.22.0
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/ponkey364/golesh-chat v1.1.2
)

retract (
	// v2.2.2 didn't do what I intended it to do.
	v2.2.2
	// v2.2.1 contained a half committed feature. should be fine with v2.2.2
	v2.2.1
)
