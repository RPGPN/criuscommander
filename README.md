# Crius Commander

It handles some discord stuff - mostly commands and also reactions.
It'll get updates as I need them for projects.

This means it'll be fairly stable for you to link against for CfDX plugins

It comes with one (1) built-in command - help.

For now, it doesn't support subcommands, but I'm seriously considering it for sometime in the near future for Secret Hitler Bot