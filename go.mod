module gitlab.com/crius-bots/criuscommander

go 1.13

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/sirupsen/logrus v1.6.0
)
