package criuscommander

import (
	"errors"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"plugin"
)

type CCConfig struct {
	Prefix              string
	Token               string
	URL                 *string
	HelpEmbedFooterText *string
	HelpEmbedColour     *int
}

type CommandGroup interface {
	OnReaction(*discordgo.Session, interface{}) error
	OnGuildJoin(*discordgo.Session, *discordgo.GuildCreate) error
	OnGuildLeave(*discordgo.Session, *discordgo.GuildDelete) error
	OnBotClose() error
}

type CommandGroupContainer struct {
	cg       CommandGroup
	commands map[string]*Command
	info     string
}

type Command struct {
	Handler CommandHandler
	Help    string
}

type CommandHandler func(*MessageContext, []string) error
type RegisterCommand func(string, CommandHandler, string) error
type GetPlugin func(string) (interface{}, error)

type Commander struct {
	client        *discordgo.Session
	commandGroups map[string]*CommandGroupContainer
	commands      map[string]*Command
	OnError       func(error)
	ccConfig      *CCConfig
}

func (c *Commander) handleError(err error) {
	logrus.Error(err)
}

func (c *Commander) RegisterCommand(groupName string) func(activator string, handler CommandHandler, help string) error {
	return func(activator string, handler CommandHandler, help string) error {
		if _, ok := c.commands[activator]; ok {
			return errors.New("a command with this name is already registered")
		}

		command := &Command{
			Handler: handler,
			Help:    help,
		}

		c.commands[activator] = command
		c.commandGroups[groupName].commands[activator] = command

		return nil
	}
}

func (c *Commander) GetPlugin(name string) (interface{}, error) {
	if pl, ok := c.commandGroups[name]; ok {
		return pl.cg, nil
	}

	return nil, errors.New("no command group found")
}

// Attempt to load a .so plugin from the disk and set it up
func (c *Commander) LoadPlugin(name string) error {
	if _, ok := c.commandGroups[name]; ok {
		return errors.New("a command group with this name is already registered")
	}

	pl, err := plugin.Open("bin/plugins/" + name + ".so")
	if err != nil {
		return err
	}

	sym, err := pl.Lookup("Setup")
	if err != nil {
		return err
	}

	c.SetupPlugin(sym.(func(RegisterCommand, GetPlugin, *discordgo.Session) (CommandGroup, string)), name)

	return nil
}

// Register the plugin into the list of 'groups' (read: plugins)
func (c *Commander) SetupPlugin(setup func(RegisterCommand, GetPlugin, *discordgo.Session) (CommandGroup, string), name string) {
	c.commandGroups[name] = &CommandGroupContainer{
		cg:       nil,
		commands: make(map[string]*Command),
	}

	group, groupInfo := setup(c.RegisterCommand(name), c.GetPlugin, c.client)
	c.commandGroups[name].cg = group
	c.commandGroups[name].info = groupInfo

	logrus.Infof("Loaded plugin %s", name)
}

func (c *Commander) HandleError(f func(error)) {
	if f != nil {
		c.OnError = f
		return
	}
	c.OnError = c.handleError
}

func (c *Commander) Open() error {
	err := c.client.Open()
	return err
}

// Stop the commander & discord
func (c *Commander) Close() {
	// call all the CommandGroup:OnBotClose
	for _, v := range c.commandGroups {
		err := v.cg.OnBotClose()
		if err != nil {
			c.OnError(err)
		}
	}

	// stop discord
	c.client.Close()
}

// Make a new instance of a commander, which will contain a connection to discord.
func NewCommander(config *CCConfig) (*Commander, error) {
	discord, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		return nil, err
	}

	cmder := &Commander{
		ccConfig:      config,
		client:        discord,
		commandGroups: make(map[string]*CommandGroupContainer),
		commands:      make(map[string]*Command),
	}

	cmder.OnError = cmder.handleError

	// set up the help command
	command := &Command{
		Handler: cmder.Help,
		Help:    "Get some help",
	}
	cmder.commands["help"] = command

	discord.AddHandler(cmder.reactionAdd)
	discord.AddHandler(cmder.reactionRemove)
	discord.AddHandler(cmder.messageCreate)
	discord.AddHandler(cmder.guildJoin)
	discord.AddHandler(cmder.guildLeave)

	return cmder, nil
}
