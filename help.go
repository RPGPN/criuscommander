package criuscommander

import (
	"github.com/bwmarrin/discordgo"
	"sort"
)

func (c *Commander) Help(m *MessageContext, args []string) error {
	var (
		embedFields []*discordgo.MessageEmbedField
		embedTitle  string
	)

	infoField := &discordgo.MessageEmbedField{
		Name:   "Info",
		Value:  "You can run `" + c.ccConfig.Prefix + "help [Command Group / Command Name]` to get help with a command or group",
		Inline: false,
	}

	embedFields = append(embedFields, infoField)

	if len(args) == 0 {
		embedTitle = "Help"

		// sort the keys into alphabetical just so it looks nicer
		keys := []string{}
		for k := range c.commandGroups {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		// now add all the fields
		for _, k := range keys {
			cg := c.commandGroups[k]

			field := &discordgo.MessageEmbedField{
				Name:   k,
				Value:  cg.info,
				Inline: false,
			}

			embedFields = append(embedFields, field)
		}
	}
	if len(args) == 1 {
		if group, ok := c.commandGroups[args[0]]; ok {
			// we've got a command group
			helpItems := group.commands

			embedTitle = args[0] + " Help"

			// sort the keys into alphabetical just so it looks nicer
			keys := []string{}
			for k := range helpItems {
				keys = append(keys, k)
			}
			sort.Strings(keys)

			// now add all the fields
			for _, k := range keys {
				v := helpItems[k] // grab the element

				field := &discordgo.MessageEmbedField{
					Name:   c.ccConfig.Prefix + k,
					Value:  v.Help,
					Inline: false,
				}

				embedFields = append(embedFields, field)
			}
		}
		// okay, maybe this is a command?
		if cmd, ok := c.commands[args[0]]; ok {
			// it is, noice
			embedTitle = args[0] + " Help"

			field := &discordgo.MessageEmbedField{
				Name:   c.ccConfig.Prefix + args[0],
				Value:  cmd.Help,
				Inline: false,
			}

			embedFields = append(embedFields, field)
		}
	}

	footerText := c.client.State.User.Username + " help generated with Crius Commander"
	if c.ccConfig.HelpEmbedFooterText != nil {
		footerText = *c.ccConfig.HelpEmbedFooterText
	}

	authorURL := "https://gitlab.com/crius-bots/criuscommander"
	if c.ccConfig.URL != nil {
		authorURL = *c.ccConfig.URL
	}

	colour := 16098851
	if c.ccConfig.HelpEmbedColour != nil {
		colour = *c.ccConfig.HelpEmbedColour
	}

	baseEmbed := &discordgo.MessageEmbed{
		Title: embedTitle,
		Color: colour,
		Footer: &discordgo.MessageEmbedFooter{
			Text: footerText,
		},
		Author: &discordgo.MessageEmbedAuthor{
			Name: c.client.State.User.Username,
			URL:  authorURL,
		},
		Fields: embedFields,
	}

	_, err := m.Send(&discordgo.MessageSend{
		Embed: baseEmbed,
	})
	if err != nil {
		return err
	}

	return nil
}
